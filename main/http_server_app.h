#ifndef __HTTP_SERVER_APP_H
#define __HTTP_SERVER_APP_H

void stop_webserver(void);
void start_webserver(void);

#endif